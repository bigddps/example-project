<?php
require_once("DbWrapper.php");

/**
 * Catalogue class, only used for displaying the whole catalogue in index.php
 *
 * @author deniels.pankins
 * @since 2019-07-20
 * @version 1
 * 
 * @property    string      $tableName
 * 
 */
class Catalogue 
{
    public $tableName = 'item';
    
    /**
     * Get the item from db using $query.
     * 
     * @author deniels.pankins
     * @since 2019-07-20
     * @version 1
     * 
     * @param string    $query      The query itself.
     * @param string    $params     Additional parameters.
     * 
     */
    public function get($query, $params) {
        $db = new db;
        return $conn = $db->select($query, $this->tableName, $params);

    }
}

