<?php
require_once('ClassLib.php');

/**
 * Type model class.
 * 
 * This class is used for getting the actual name of the type from db using the ID.
 *
 * @author deniels.pankins
 * @since 2019-07-20
 * @version 1
 * 
 * @property    string  $tableName      Name of the table in db.
 * @property    int     $typeID         
 */
class TypeModel 
{
    public $tableName = 'item_type';

    public function validate($type) {
        $validator = new Validator();
        try {
            $validator->isExist($type, "Item type");
            $validator->isInRange($type, 1, 3, "Item type");
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            echo (
                $errorMessage . 
                "<a href = \"index.php\">ah shit go bacc</a>"
                );
            return false;
        }
        return true;
    }
    public function get($type) {
        $query = "name";
        $params = "ID='" . $type . "'";
        $db = new db;
        $conn = $db->select($query, $this->tableName, $params);
        $result = mysqli_fetch_row($conn);
        return $result[0];
    }
}


