<?php
require_once('ClassLib.php');

/**
 * Child class for books.
 * 
 * @inheritdoc
 * 
 */
class ItemBook extends Item
{
    public $weight;

    public function save() {
        $item = array($this->sku, $this->name, $this->price, $this->type, $this->weight);
        $values = implode("', '", $item);
        $values = "'" . $values . "'";
        $sql = 
                "INSERT INTO 
                $this->tableName 
                (sku, name, price, type, attr_weight) 
                VALUES ($values)
                ";
        $db = new db;
        $conn = $db->insert($sql);
    }

    public function validate($validator = NULL) {
        $validator = new Validator();
        $parentValidation = parent::validate($validator);
        if ($parentValidation != false) {
            try {
                $validator->isExist($this->weight, "Weight");
                $validator->isANumber($this->weight, "Weight");
                $validator->isInLength($this->weight, 20, "Weight");
            } catch (Exception $e) {
                $errorMessage = $e->getMessage();
                echo (
                    $errorMessage . 
                    "<a href = \"index.php\">ah shit go bacc</a>"
                    );
                return false;
            }
            return true;
        } else {
            return false;
        }
        
    }

}