<?php
require_once('ClassLib.php');

/**
 * Child class for Furniture.
 * 
 * @inheritdoc
 * 
 */
class ItemFurniture extends Item
{
    public $height;
    public $width;
    public $length;

    public function save() {
        $item = array($this->sku, $this->name, $this->price, $this->type, $this->height, $this->width, $this->length);
        $values = implode("', '", $item);
        $values = "'" . $values . "'";
        $sql = 
                "INSERT INTO 
                $this->tableName 
                (sku, name, price, type, attr_dimension_h, attr_dimension_w, attr_dimension_l) 
                VALUES ($values)
                ";
        $db = new db;
        $conn = $db->insert($sql);
    }

    public function validate($validator = NULL) {
        $validator = new Validator();
        $parentValidation = parent::validate($validator);
        if ($parentValidation != false) {
            try {
                $validator->isExist($this->height, "Height");
                $validator->isANumber($this->height, "Height");
                $validator->isInLength($this->height, 20, "Height");

                $validator->isExist($this->width, "Width");
                $validator->isANumber($this->width, "Width");
                $validator->isInLength($this->width, 20, "Width");

                $validator->isExist($this->length, "Length");
                $validator->isANumber($this->length, "Length");
                $validator->isInLength($this->length, 20, "Length");
            } catch (Exception $e) {
                $errorMessage = $e->getMessage();
                echo (
                    $errorMessage . 
                    "<a href = \"index.php\">ah shit go bacc</a>"
                    );
                return false;
            }
            return true;
        } else {
            return false;
        }
    }
}