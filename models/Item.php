<?php
require_once('ClassLib.php');
include("models/ItemDVDDisk.php");
include("models/ItemBook.php");
include("models/ItemFurniture.php");
/**
 * Item class
 * 
 * Prepares and operates user input to save the item into db.
 * This is a generic class, not used directly, 
 * child classes mostly include overrides of existing methods due to their differences in attributes.
 *
 * @author deniels.pankins
 * @since 2019-07-20
 * @version 1
 * 
 * @property    string  $sku
 * @property    string  $name
 * @property    string  $price
 * @property    string  $type
 * @property    string  $tableName      Name of the table in db.
 * 
 */
abstract class Item
{
    public $sku;
    public $name;
    public $price;
    public $type;
    public $tableName = 'item';

    /**
     * Set class properties using a passed parameter.
     * 
     * @author deniels.pankins
     * @since 2019-07-20
     * @version 1
     * @param   array   $attributes
     * 
     */
    public function setAttributes($attributes) {
        foreach ($attributes as $name => $value) {
            if (property_exists($this, $name)) {
                $this->{$name} = $value;
            } else {
                $this->{$name} = NULL;
            }
        }
    }
    
    /**
     *  Validate properties, catch an exception if data is incorrect.
     *  
     * @author deniels.pankins
     * @since 2019-07-20
     * @version 1
     * 
     * @return mixed
     * 
     */
    public function validate($validator) {
        try {
            $validator->isExist($this->sku, "SKU");
            $validator->isInLength($this->sku, 20, "SKU");
            
            $validator->isExist($this->name, "Name");
            $validator->isInLength($this->name, 40, "Name");

            $validator->isExist($this->price, "Price");
            $validator->isANumber($this->price, "Price");
            $validator->isInLength($this->price, 15, "Price");

            $validator->isExist($this->type, "Item type");
            $validator->isInRange($this->type, 1, 3, "Item type");
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            echo (
                $errorMessage . 
                "<a href = \"index.php\">ah shit go bacc</a>"
                );
            return false;
        }
        return true;
        
    }

    /**
     *  Prepare class properties for insertion into db, insert them.
     *  This method is always overridden in child classes because of his
     *  value intersection and thus unability to properly use parent's method.
     *  
     * @author deniels.pankins
     * @since 2019-07-20
     * @version 1
     * 
     */
    public function save() {
        $item = array($this->sku, $this->name, $this->price, $this->type);
        $values = implode("', '", $item);
        $values = "'" . $values . "'";
        $sql = 
                "INSERT INTO 
                $this->tableName 
                (sku, name, price, type) 
                VALUES ($values)
                ";
        $db = new db;
        $conn = $db->insert($sql);
    }
}
