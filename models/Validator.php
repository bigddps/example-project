<?php

/**
 * Validator class
 * 
 * Set of methods to check for certain conditions of passed data.
 *
 * @author deniels.pankins
 * @since 2019-07-20
 * @version 1
 * 
 */
class Validator
{
   /**
    * Check if a passed parameter is set, 
    * throw an exception if not set.
    * 
    * @author deniels.pankins
    * @since 2019-07-20
    * @version 1
    *
    * @param string         $value              Checked parameter itself.
    * @param string         $checkedParam       Parameter's "name".
    */
    public function isExist($value, $checkedParam) {
        if (isset($value) == false)
            throw new Exception($checkedParam . " is not set!");
    }

   /**
    * Check if a passed parameter's length is more than $maxLength passed parameter's value,
    * throw an exception if true.
    * 
    * @author deniels.pankins
    * @since 2019-07-20
    * @version 1
    *
    * @param string     $value              Checked parameter itself.
    * @param int        $maxLength          Maximum acceptable length.
    * @param string     $checkedParam       Parameter's "name".
    *
    */
    public function isInLength($value, $maxLength, $checkedParam) {
        $result = strlen($value);
        if ($result == 0) {
            throw new Exception($checkedParam . " is empty!");
        } else if ($result > $maxLength) {
            throw new Exception($checkedParam . " is longer than " . $maxLength . " symbols.");
        }
    }

   /**
    * Check if a passed parameter is in range of X ($minValue) and Y ($maxValue),
    * throw an exception if false.
    * 
    * @author deniels.pankins
    * @since 2019-07-20
    * @version 1
    *
    * @param int        $value              Checked parameter itself.
    * @param int        $minValue           Minimum acceptable value.
    * @param int        $maxValue           Maximum acceptable value
    * @param string     $checkedParam       Parameter's "name".
    *
    */
    public function isInRange($value, $minValue, $maxValue, $checkedParam) {
        if ($value < $minValue || $value > $maxValue) {
            throw new Exception($checkedParam . " is not in range.");
        }
    }
   /**
    * Check if a passed parameter is a number,
    * throw an exception if false.
    * 
    * @author deniels.pankins
    * @since 2019-11-08
    * @version 1
    *
    * @param int        $value              Checked parameter itself.
    * @param string     $checkedParam       Parameter's "name".
    *
    */
    public function isANumber($value, $checkedParam) {
        if (is_numeric($value) != true) {
            throw new Exception($checkedParam . " is not a number.");
        }
    }
}