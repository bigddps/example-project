<?php
require_once('ClassLib.php');

/**
 * Child class for DVD discs.
 * 
 * @inheritdoc
 * 
 */
class ItemDVDDisk extends Item
{
    public $size;

    public function save() {
        $item = array($this->sku, $this->name, $this->price, $this->type, $this->size);
        $values = implode("', '", $item);
        $values = "'" . $values . "'";
        $sql = 
                "INSERT INTO 
                $this->tableName 
                (sku, name, price, type, attr_size) 
                VALUES ($values)
                ";
        $db = new db;
        $conn = $db->insert($sql);
    }

    public function validate($validator = NULL) {
        $validator = new Validator();
        $parentValidation = parent::validate($validator);
        if ($parentValidation != false) {
            try {
                $validator->isExist($this->size, "Size");
                $validator->isANumber($this->size, "Size");
                $validator->isInLength($this->size, 20, "Size");
            } catch (Exception $e) {
                $errorMessage = $e->getMessage();
                echo (
                    $errorMessage . 
                    "<a href = \"index.php\">ah shit go bacc</a>"
                    );
                return false;
            }
            return true;
        } else {
            return false;
        } 
    }
}