// Function which is triggered when type form is changed, sends a GET request to the getForm.php document.

function getForm(val) {
    var xhttp = new XMLHttpRequest();
    if (0 == val) {
        document.getElementById("placeholder").innerHTML = "";
    }
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
           document.getElementById("placeholder").innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "getForm.php?type="+val, true);
    xhttp.send();
}