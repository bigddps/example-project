<?php 

/**
 * Includes of PHP documents.
 * 
 * @author deniels.pankins
 * @since 2019-07-20
 * @version 1
 * 
 * @todo    Migrate to autoloader.
 */
include("DbWrapper.php");
include("models/TypeModel.php");
include("models/Catalogue.php");
include("models/Validator.php");
include("models/Item.php");

