<?php require_once('ClassLib.php');?>

<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="stylesheets/css/index.css" media="screen" />
</head>
<body>
    <div class = "header">
        <p class = "title">Product List</p>
    </div>
    <a href = "addItem.php">add item</a>
    <div class = "product_list">
        <?php 
            $query = '';
            $params = '';
            $catalogue = new Catalogue;
            $result = $catalogue->get($query, $params);
            if ($result->num_rows > 0) {
                  // output data of each row
                while($row = $result->fetch_array()) {
                    $attr = "";
                    $attr_measure_type = "";
                    $price = substr_replace($row["price"], '.', -2, 0);
                    switch ($row["type"]) {
                        case 1:
                            $attr_name = "Size: ";
                            $attr = $row["attr_size"];
                            $attr_measure_type = " MB";
                            break;
                        case 2:
                            $attr_name = "Weight: ";
                            $attr = $row["attr_weight"];
                            $attr_measure_type = " KG";
                            break;
                        case 3:
                            $attr_name = "Dimension: ";
                            $attr = $row["attr_dimension_h"] . 'x' . $row["attr_dimension_w"] . 'x' . $row["attr_dimension_l"];
                            break;
                    }
                    echo 
                        '<div class = "grid_cell">' . 
                            '<div>' . $row["sku"] . '</div>' . '<br>' .
                            '<div>' . $row["name"] . '</div>' . '<br>' .
                            '<div>' . $price . ' $</div>' . '<br>' .
                            '<div>' . $attr_name . $attr . $attr_measure_type . '</div>' . '<br>' .
                        '</div>';
                }
            } else {
            echo "0 results";
            }
        ?>

    </div>
</body>
</html>