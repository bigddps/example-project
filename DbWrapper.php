<?php

/**
 * Database class.
 * 
 * A class with a set of methods operating with the database.
 * 
 * 
 * @author deniels.pankins
 * @since 2019-07-20
 * @version 1
 * 
 * @method          object      connect()                               Creates a connection to a MySQL Server.
 * @method          object      select(query, $tableName, $params)      Get data from the db.
 * @method          object      insert($sql)                            Inserts data into db.
 * 
 */
class db
{

   /**
    *    Connect to the db.
    *
    * @author deniels.pankins
    * @since 2019-07-20
    * @version 1
    *
    * @return   object
    *
    */
    public function connect() 
    {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $database = "product_list";
        return new mysqli($servername, $username, $password, $database);
    }

   /**
    *     Get data from the db.
    *
    * @author deniels.pankins
    * @since 2019-07-20
    * @version 1
    *
    * @param    string      $query      Data to SELECT.
    * @param    string      $tableName  Name of the table to get data from.
    * @param    string      $params     Additional parameters for the SQL query.
    * @return   object
    *
    */
    public function select($query, $tableName, $params) 
    {
        if ($query == null)
        {
            $query = '*';
        }
        $conn = $this->connect();
        if ($params != null) 
        {
            $sql = "SELECT $query FROM $tableName WHERE $params";
            return $conn->query($sql);
        }
        $sql = "SELECT $query FROM $tableName";
        return $conn->query($sql);
    }

   /**
    *    Add data to the db.
    *
    * @author deniels.pankins
    * @since 2019-07-20
    * @version 1
    *
    * @param    string      $sql    SQL query.
    * @return   object      
    * 
    */
    public function insert($sql) {
        $conn = $this->connect();
        $conn->query($sql);
    }


}
