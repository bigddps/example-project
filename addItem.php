<?php require_once('ClassLib.php');?>

<!DOCTYPE html>
<html>
<!--
    Webpage for submitting new items. 
-->
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="javascript/changeForm.js"></script>
    <link rel="stylesheet" type="text/css" href="stylesheets/css/addItem.css" media="screen" />

</head>
<body>
    <div class = "header">
        <p class = "title">Product add</p>
        <button type = "submit" form = "submit_form">Save</button>
    </div>    
    <a href = "index.php">ah shit go bacc</a>
    <div class = "product_add_form">
        <form action="submit.php" method="post" id="submit_form">

            <div class = "sku-form">
                <label for="sku">SKU</label> 
                <input type="text" name="sku"><br>
            </div>

            <div class = "name-form">
                <label for="name">Name</label>
                <input type="text" name="name"><br>
            </div>

            <div class = "price-form">
                <label for="price">Price</label>
                <input type="text" name="price"><br>
                <p>Please specify the price including the 2 positions after a dot, i.e. "2000" for "20.00 $"</p>
            </div>

            <div class = "type-form">
                <label for="type">Type</label>
                <select name="type" onchange="getForm(this.value)">
                    <option value="" selected disabled hidden>Select type...</option>
                    <option value="1">DVD Disk</option>
                    <option value="2">Book</option>
                    <option value="3">Furniture</option>
                </select><br>
            </div>

            <div id="product-attribute-form">
                <div id="placeholder">
                </div>
            </div>

        </form>
    </div>
    </form>
</body>
</html>