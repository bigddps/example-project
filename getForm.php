<?php
/**
 * A script echoing a HTML form for a special attribute when item type is changed.
 * 
 * @author deniels.pankins
 * @since 2019-07-20
 * @version 1
 * 
 */

$type = $_REQUEST["type"];
switch ($type) {
    case 1:
        echo
            '<div class = "size-form">' . 
                '<label for="size">Size</label>' . 
                '<input type="text" name="size"><br>' . 
            '<p>' . 
                'Please provide the size in megabytes.' . 
            '</p>' . 
            '</div>';
    break;
    case 2:
        echo
            '<div class = "weight-form">' . 
                '<label for="weight">Weight</label>' . 
                '<input type="text" name="weight"><br>' . 
            '<p>' . 
                'Please provide the weight in kilograms.' . 
            '</p>' .
            '</div>';
    break;
    case 3:
        echo    
            '<div class = "height-form">' . 
                '<label for="height">Height</label>' .
                '<input type="text" name="height"><br>' .
            '</div>' . 
            '<div class = "width-form">' . 
                '<label for="width">Width</label>' . 
                '<input type="text" name="width"><br>' . 
            '</div>' .
            '<div class = "length-form">' . 
                '<label for="length">Length</label>' . 
                '<input type="text" name="length"><br>' .
            '</div>' . 
            '<p>' . 
                'Please provide dimensions in HxWxL format, in centimeters.' .
            '</p>';
    break;
}