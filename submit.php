<?php
require_once("ClassLib.php");

/**
 * Submitting data to the server.
 */

$type = $_POST["type"];
$typeModel = new TypeModel;
if ($typeModel->validate($type) != false) {
    $typeName = $typeModel->get($type);
    $item_class = "Item" . $typeName;
    $item = new $item_class;
    $item->setAttributes($_POST);
    if ($item->validate() != false) {
        $item->save();
        echo("Uploaded succsessfully, redirecting you back in 3 seconds!");
        header('Refresh: 3; URL=index.php');
    }
}


