-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for product_list
CREATE DATABASE IF NOT EXISTS `product_list` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `product_list`;

-- Dumping structure for table product_list.item
CREATE TABLE IF NOT EXISTS `item` (
  `sku` varchar(20) NOT NULL,
  `name` varchar(60) NOT NULL,
  `price` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `attr_size` int(10) unsigned DEFAULT NULL COMMENT 'Size in mb for disk type',
  `attr_weight` int(10) unsigned DEFAULT NULL COMMENT 'Weight in kg for type book',
  `attr_dimension_h` int(10) unsigned DEFAULT NULL COMMENT 'Height dimension in mm for type furniture',
  `attr_dimension_w` int(10) unsigned DEFAULT NULL COMMENT 'Weight dimension in mm for type furniture',
  `attr_dimension_l` int(10) unsigned DEFAULT NULL COMMENT 'Length dimension in mm for type furniture',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sku`),
  KEY `FK_item_type_to_item_c_type` (`type`),
  CONSTRAINT `FK_item_type_to_item_c_type` FOREIGN KEY (`type`) REFERENCES `item_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table product_list.item: ~9 rows (approximately)
DELETE FROM `item`;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` (`sku`, `name`, `price`, `type`, `attr_size`, `attr_weight`, `attr_dimension_h`, `attr_dimension_w`, `attr_dimension_l`, `created_at`, `updated_at`) VALUES
	('BG74127', 'Quartz Table', 30000, 3, NULL, NULL, 101, 233, 220, '2019-11-08 20:25:25', '2019-11-08 20:35:57'),
	('HK20E', 'Black Death', 2099, 2, NULL, 2, NULL, NULL, NULL, '2019-06-13 22:09:57', '2019-06-13 22:09:57'),
	('JP8571', 'Astral Chain OST', 5999, 1, 4857, NULL, NULL, NULL, NULL, '2019-11-08 20:38:49', '2019-11-08 20:38:49'),
	('LV22892', 'Zhizn Shprot', 1337, 1, 2192, NULL, NULL, NULL, NULL, '2019-06-16 02:25:34', '2019-11-08 20:36:27'),
	('RU11U', 'Russian Taburetka', 10000, 3, NULL, NULL, 2000, 5000, 1000, '2019-06-13 22:12:23', '2019-11-08 20:36:03'),
	('RU2002', 'Michael Circle Best hits', 999, 1, 782, NULL, NULL, NULL, NULL, '2019-11-08 21:11:35', '2019-11-08 21:11:35'),
	('TR120555', 'Chair', 4000, 3, NULL, NULL, 24, 45, 16, '2019-11-08 21:09:06', '2019-11-08 21:09:06'),
	('TR28569', 'Secrets of Kebab', 1999, 2, NULL, 1, NULL, NULL, NULL, '2019-11-08 20:37:44', '2019-11-08 20:37:44'),
	('US2020', 'Elections 101', 1000, 1, 742, NULL, NULL, NULL, NULL, '2019-11-08 20:26:46', '2019-11-08 20:35:55'),
	('US92I', 'Matrix 2', 1300, 1, 6174, NULL, NULL, NULL, NULL, '2019-06-13 22:11:14', '2019-11-08 20:36:13'),
	('XD5728', 'Harry Potny', 2100, 2, NULL, 20, NULL, NULL, NULL, '2019-07-18 01:56:07', '2019-11-08 20:35:59');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;

-- Dumping structure for table product_list.item_type
CREATE TABLE IF NOT EXISTS `item_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Item type definition\r\n';

-- Dumping data for table product_list.item_type: ~2 rows (approximately)
DELETE FROM `item_type`;
/*!40000 ALTER TABLE `item_type` DISABLE KEYS */;
INSERT INTO `item_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'DVDDisk', '2019-06-13 22:08:44', '2019-07-24 02:52:46'),
	(2, 'Book', '2019-06-13 22:08:55', '2019-07-24 02:52:08'),
	(3, 'Furniture', '2019-06-13 22:09:06', '2019-07-24 02:52:12');
/*!40000 ALTER TABLE `item_type` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
